package assignments.assignment3;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    String nama;
    
    int kapasitas;

    Dosen dosen;

    Mahasiswa[] daftarMahasiswa;
    int jumlahMahasiswa = 0;

    public String getNama() {
        return this.nama;
    }
    public Dosen getDosen() {
        return this.dosen;
    }
    public int getKapasitas() {
        return this.kapasitas;
    }
    public int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMahasiswa[jumlahMahasiswa++] = mahasiswa;
    }

    void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int x=0; x<jumlahMahasiswa; x++) { // cari mahasiswa
            if (daftarMahasiswa[x].equals(mahasiswa)) {
                daftarMahasiswa[x] = null; // apus mahasiswa
                jumlahMahasiswa--;
                break;
            }
        }

        // buat daftarmahasiswa baru biar ga bolong bolong isinya, karena udh ada yg dihapus (null) --> {a, b, null, c, d, null, null, null, ...}
        // jd buat daftar baru yang terurut --> {a, b, c, d, null, null, null, ...}
        Mahasiswa[] baru = new Mahasiswa[kapasitas];
        int index = 0; // index buat urutan daftar baru
        for (int y=0; y<kapasitas; y++) { // y sebagai index daftar lama
            if (daftarMahasiswa[y] != null) {
                baru[index++] = daftarMahasiswa[y];
            }
        }

        daftarMahasiswa = baru;

    }

    void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = dosen;
    }

    void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = null;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public boolean equals(MataKuliah other) {
        return (this.nama.equals(other.getNama()));
    }
}