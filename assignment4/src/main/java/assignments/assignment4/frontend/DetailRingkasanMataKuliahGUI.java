package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    private JLabel labelJudul = new JLabel("Detail Ringkasan Mata Kuliah");
    private JLabel labelNama = new JLabel();
    private JLabel labelKode = new JLabel();
    private JLabel labelSKS = new JLabel();
    private JLabel labelJumlah = new JLabel();
    private JLabel labelKapasitas = new JLabel();
    private JLabel labelDaftarMahasiswa = new JLabel("Daftar Mahasiswa:");
    private JLabel labelMahasiswaGaAda = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
    
    private JButton jbtSelesai = new JButton("Selesai");

    private JPanel panelMahasiswa = new JPanel();
    private JPanel panelSelesai = new JPanel();

    private Font fontUrutan = new Font("Times New Roman", Font.BOLD, 11);


    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        setPanel(frame, mataKuliah);
        setKomponen(mataKuliah, daftarMahasiswa, daftarMataKuliah);
        setListener(frame, daftarMahasiswa, daftarMataKuliah);

    }

    private void setPanel(JFrame frame, MataKuliah mataKuliah) {
        JPanel frameTengah = new JPanel();
        frameTengah.setLayout(new GridLayout(9, 1, 0, 0)); // 9 rows and 1 column
        frame.setLayout(new BorderLayout(0, 30)); //vgap = 30

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        frameTengah.add(labelJudul);
        frameTengah.add(labelNama);
        frameTengah.add(labelKode);
        frameTengah.add(labelSKS);
        frameTengah.add(labelJumlah);
        frameTengah.add(labelKapasitas);
        frameTengah.add(labelDaftarMahasiswa);
        frameTengah.add(panelMahasiswa);
        frameTengah.add(panelSelesai);

        setUrutanMahasiswa(mataKuliah);
        
        panelSelesai.add(jbtSelesai);
    }

    private void setUrutanMahasiswa(MataKuliah mataKuliah) {
        if (mataKuliah.getJumlahMahasiswa() != 0) {
            int row = mataKuliah.getJumlahMahasiswa();
            panelMahasiswa.setLayout(new GridLayout(row, 1, 0, 5));

            String[] urutanMatkul = getUrutanMahasiswa(mataKuliah);
            int nomor = 1;
            String namaMatkul; // 1. DDP --Next loop--> 2.Matdis --> ...
            for (int x=0; x<urutanMatkul.length; x++) {
                namaMatkul = (nomor++) + ". " + urutanMatkul[x];
                JLabel text = new JLabel(namaMatkul);
                text.setHorizontalAlignment(JLabel.CENTER);
                text.setFont(fontUrutan);
                panelMahasiswa.add(text);
            }
        } else {
            panelMahasiswa.add(labelMahasiswaGaAda);
            labelMahasiswaGaAda.setFont(fontUrutan);
        }
    }

    private String[] getUrutanMahasiswa(MataKuliah mataKuliah) {
        Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();
        String[] daftarNamaMahasiswa = new String[mataKuliah.getJumlahMahasiswa()];
        String[] deepCopy = new String[mataKuliah.getJumlahMahasiswa()];

        for (int x=0; x<mataKuliah.getJumlahMahasiswa(); x++) { // mengisi nilai array
            daftarNamaMahasiswa[x] = daftarMahasiswa[x].getNama();
            deepCopy[x] = daftarMahasiswa[x].getNama();
        }

        String[] urutanNamaMahasiswa = new String[mataKuliah.getJumlahMahasiswa()]; // daftarNamaMahasiswa versi terurut secara alfabet

        String min = deepCopy[0];
        int indexMinSementara = 0;
        for (int y=0; y<urutanNamaMahasiswa.length; y++) { // ngurutin dr alfabet terkecil ke besar. ex: a, b, c, ...
            for (int x=0; x<deepCopy.length; x++) { // mencari nama terkecil dalam array deepCopy di setiap loop
                if ((deepCopy[x] != null) && (deepCopy[x].compareToIgnoreCase(min) < 0)) {
                    min = deepCopy[x];
                    indexMinSementara = x;
                }
            }
            deepCopy[indexMinSementara] = null; // apus
            urutanNamaMahasiswa[y] = min;
            
            int index = 0;
            min = deepCopy[index];
            while ((index<deepCopy.length) && (min == null)) { // set min ke elemen yg belom di apus (dijadiin null)
                min = deepCopy[index++];
            }
        }

        return urutanNamaMahasiswa;

    }

    private void setKomponen(MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        String nama = "Nama mata kuliah: " + mataKuliah.getNama();
        String kode = "Kode: " + mataKuliah.getKode();
        String sks = "SKS: " + mataKuliah.getSKS();
        String jumlahMahasiswa = "Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa();
        String kapasitas = "Kapasitas: " + mataKuliah.getKapasitas();

        labelNama.setText(nama);
        labelKode.setText(kode);
        labelSKS.setText(sks);
        labelJumlah.setText(jumlahMahasiswa);
        labelKapasitas.setText(kapasitas);
        
        labelJudul.setFont(SistemAkademikGUI.fontTitle);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        labelKode.setFont(SistemAkademikGUI.fontGeneral);
        labelSKS.setFont(SistemAkademikGUI.fontGeneral);
        labelJumlah.setFont(SistemAkademikGUI.fontGeneral);
        labelKapasitas.setFont(SistemAkademikGUI.fontGeneral);
        labelDaftarMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        jbtSelesai.setFont(SistemAkademikGUI.fontGeneral);

        labelJudul.setHorizontalAlignment(JLabel.CENTER);
        labelNama.setHorizontalAlignment(JLabel.CENTER);
        labelKode.setHorizontalAlignment(JLabel.CENTER);
        labelSKS.setHorizontalAlignment(JLabel.CENTER);
        labelJumlah.setHorizontalAlignment(JLabel.CENTER);
        labelKapasitas.setHorizontalAlignment(JLabel.CENTER);
        labelDaftarMahasiswa.setHorizontalAlignment(JLabel.CENTER);

        jbtSelesai.setBackground(SistemAkademikGUI.bg1);
        jbtSelesai.setForeground(SistemAkademikGUI.fg);
    }
    
    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtSelesai.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }


}
