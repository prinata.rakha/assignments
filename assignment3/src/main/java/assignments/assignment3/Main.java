package assignments.assignment3;

import java.util.*;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    static int totalMataKuliah = 0;

    static int totalElemenFasilkom = 0; 

    static void addMahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom mahasiswa = new Mahasiswa(nama, npm);
        daftarElemenFasilkom[totalElemenFasilkom++] = mahasiswa;
        System.out.println(nama + " berhasil ditambahkan");

    }

    static void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom dosen = new Dosen(nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = dosen;
        System.out.println(nama + " berhasil ditambahkan");

    }

    static void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom elemenKantin = new ElemenKantin(nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = elemenKantin;
        System.out.println(nama + " berhasil ditambahkan");

    }

    static void menyapa(String objek1, String objek2) {
        /* TODO: implementasikan kode Anda di sini */
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
            return;
        }

        ElemenFasilkom obj1 = null;
        ElemenFasilkom obj2 = null;
        for (int x=0; x<totalElemenFasilkom+1; x++) {   // biar ga off by one error, ditambah 1 
            if ((obj1 != null) && (obj2 != null)) {     // objek udh ditemukan
                if (obj1.equals(obj2)) {
                    System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
                } else {
                    obj1.menyapa(obj2);
                    // obj2.menyapa(obj1);
                }
                break; // stop loop
            }

            if (daftarElemenFasilkom[x].getNama().equals(objek1)) {
                obj1 = daftarElemenFasilkom[x]; // dapet objek 1
            }
            if (daftarElemenFasilkom[x].getNama().equals(objek2)) {
                obj2 = daftarElemenFasilkom[x]; // dapet objek 2
            } else ;
        }
    }

    static void addMakanan(String objek, String namaMakanan, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        for (int x=0; x<daftarElemenFasilkom.length; x++) {
            if (daftarElemenFasilkom[x].getNama().equals(objek)) {
                if (daftarElemenFasilkom[x].getTipe().equals("ElemenKantin")) {
                    ((ElemenKantin)daftarElemenFasilkom[x]).setMakanan(namaMakanan, harga);
                } else {
                    System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin%n", objek);
                }
                return;
            } else ;
        }
    }

    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj1 = null;
        ElemenFasilkom obj2 = null;

        for (int x=0; x<daftarElemenFasilkom.length; x++) { // mencari objek pembeli dan penjual
            if ((obj1 != null) && (obj2 != null)) { // kedua objek sudah ditemukan
                break;
            }
            if (daftarElemenFasilkom[x].getNama().equals(objek1)) {
                obj1 = daftarElemenFasilkom[x]; // objek pembeli ketemu
            }
            if (daftarElemenFasilkom[x].getNama().equals(objek2)) {
                if (daftarElemenFasilkom[x].getTipe().equals("ElemenKantin")) { // objek penjual ketemu
                    obj2 = daftarElemenFasilkom[x];
                } else {
                    System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan"); // penjual bukan elemen kantin
                    return;
                }
            }
        }

        if (obj1.equals(obj2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        } else {
            obj1.membeliMakanan(obj1, obj2, namaMakanan); // beli makan (berhasil)
        }
    }

    static void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah++] = matkul;
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    static void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = null;
        MataKuliah matkul = null;

        for (int x=0; x<daftarElemenFasilkom.length; x++) { // mencari objek mahasiswa dan matkul
            if ((obj != null) && (matkul != null)) { // mahasiswa dan matkulnya sudah ditemukan
                break;
            }
            if ((daftarElemenFasilkom[x] != null) && (daftarElemenFasilkom[x].getNama().equals(objek))) { // mencari nama mahasiswa
                if (daftarElemenFasilkom[x].getTipe().equals("Mahasiswa")) {
                    obj = daftarElemenFasilkom[x];
                } else {
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
                    return;
                }
            }
            if ((daftarMataKuliah[x] != null) && (daftarMataKuliah[x].getNama().equals(namaMataKuliah))) { // mencari matkul
                matkul = daftarMataKuliah[x];
            }
        }

        ((Mahasiswa)obj).addMatkul(matkul); // berhasil menambahkan matkul
        
    }

    static void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = null;
        MataKuliah matkul = null;

        for (int x=0; x<daftarElemenFasilkom.length; x++) {
            if ((obj != null) && (matkul != null)) {
                break;
            }
            if (daftarElemenFasilkom[x].getNama().equals(objek)) {
                if (daftarElemenFasilkom[x].getTipe().equals("Mahasiswa")) {
                    obj = daftarElemenFasilkom[x];
                } else {
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
                    return;
                }
            }
            if ((daftarMataKuliah[x] != null) && (daftarMataKuliah[x].getNama().equals(namaMataKuliah))) {
                matkul = daftarMataKuliah[x];
            }
        }

        ((Mahasiswa)obj).dropMatkul(matkul); // berhasil drop matkul

    }

    static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = null;
        MataKuliah matkul = null;

        for (int x=0; x<daftarElemenFasilkom.length; x++) {
            if ((obj != null) && (matkul != null)) {
                break;
            }

            if (daftarElemenFasilkom[x].getNama().equals(objek)) { // mencari dosen
                if (daftarElemenFasilkom[x].getTipe().equals("Dosen")) {
                    obj = daftarElemenFasilkom[x];
                } else { // objek bukan dosen
                    System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
                    return;
                }
            }

            if ((daftarMataKuliah[x] != null) && (daftarMataKuliah[x].getNama().equals(namaMataKuliah))) {
                matkul = daftarMataKuliah[x];
            }
        }

        ((Dosen)obj).mengajarMataKuliah(matkul);
    }

    static void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = null;

        for (int x=0; x<daftarElemenFasilkom.length; x++) {
            if (daftarElemenFasilkom[x].getNama().equals(objek)) { // mencari dosen
                if (daftarElemenFasilkom[x].getTipe().equals("Dosen")) {
                    obj = daftarElemenFasilkom[x]; // ketemu objek dosennya
                    ((Dosen)obj).dropMataKuliah(); // berhasil drop matkul
                    break;
                } else { // objek bukan dosen
                    System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
                    return;
                }
            }
        }
    }

    static void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom obj = null;

        for (int x=0; x<daftarElemenFasilkom.length; x++) {
            if (daftarElemenFasilkom[x].getNama().equals(objek)) { // nama sesuai
                if (daftarElemenFasilkom[x].getTipe().equals("Mahasiswa")) { // apakah mahasiswa?
                    obj = daftarElemenFasilkom[x];
                    Mahasiswa mhs = (Mahasiswa)obj; // casting elemenfasilkom jd mahasiswa

                    System.out.printf("Nama: %s%nTanggal lahir: %s%nJurusan: %s%nDaftar Mata Kuliah:%n", mhs.getNama(), mhs.extractTanggalLahir(mhs.getNpm()), mhs.extractJurusan(mhs.getNpm()));
                    
                    if (mhs.getJumlahMatkul() == 0) {
                        System.out.println("Belum ada mata kuliah yang diambil");
                        return;
                    }
                    
                    for (int y=0; y<mhs.getMataKuliah().length; y++) { // print list matkul (1.  , 2.  , ...)
                        int nomor = y+1;
                        if (mhs.getMataKuliah()[y] != null) {
                            System.out.println(nomor + ". " + mhs.getMataKuliah()[y].getNama());
                        }
                    }

                    break;
                } else { // nama sesuai tp bukan mahasiswa
                    System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa%n", objek);
                    return;
                }
            }
        }
    }

    static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah matkul = null;

        for (int x=0; x<daftarMataKuliah.length; x++) { // cari objek matkul
            if (daftarMataKuliah[x].getNama().equals(namaMataKuliah)) {
                matkul = daftarMataKuliah[x]; // ketemu objek matkul
                // nama, jumlah mahasiswa, kapasitas, dosen, daftar mahasiswa
                System.out.print("Nama mata kuliah: " + matkul.getNama() + 
                        "\nJumlah mahasiswa: " + matkul.getJumlahMahasiswa() +
                        "\nKapasitas: " + matkul.getKapasitas() + 
                        "\nDosen pengajar: ");
                
                // cek dosen
                if (matkul.getDosen() != null) {
                    System.out.println(matkul.getDosen().getNama());
                } else {
                    System.out.println("Belum ada");
                }

                // cek daftar mahasiswa
                System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
                if (matkul.getJumlahMahasiswa() == 0) {
                    System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
                } else { // print list mahasiswa (1.  , 2.  , ...)
                    int nomor = 1;
                    for (int y=0; y<matkul.getJumlahMahasiswa(); y++) {
                        System.out.println(nomor + ". " + matkul.getDaftarMahasiswa()[y]);
                        nomor++;
                    }
                }
                
                break;
            }
        }
    }

    static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        for (int x=0; x<totalElemenFasilkom; x++) {
            ElemenFasilkom ef = daftarElemenFasilkom[x];

            if (ef.getJumlahDisapa() >= (((totalElemenFasilkom-1)/2))) { // nyapa lebih dr stengah
                ef.friendship += 10;
            } 
            else { // ganyampe stengah yg disapa
                ef.friendship -= 5;
            }

            // friendship cmn boleh 0-100
            if (ef.friendship > 100) {
                ef.friendship = 100;
            } else if (ef.friendship < 0) {
                ef.friendship = 0;
            }

            ef.resetMenyapa();
        }

        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");

        friendshipRanking();
    }

    static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */    
        ElemenFasilkom max = new Dosen("Z"); //friendship = 0

        // nge copy daftarElemenFasilkom biar bisa di ganti ganti isinya, nantinya akan ada objek yang dihapus untuk mengurutkan objek
        ElemenFasilkom[] copyDaftar = Arrays.copyOf(daftarElemenFasilkom, totalElemenFasilkom); 
        ElemenFasilkom[] daftarUrutan = new ElemenFasilkom[totalElemenFasilkom];
        for (int y=0; y<totalElemenFasilkom; y++) { // buat array yg sudah terurut

            for (int x=0; x<totalElemenFasilkom; x++) { // mencari friendship terbesar di array copyDaftar
                if (copyDaftar[x] != null) {

                    if (copyDaftar[x].friendship > max.friendship) {
                        max = copyDaftar[x]; // ketemu
                    } else if (copyDaftar[x].friendship == max.friendship) {
                        if (copyDaftar[x].getNama().compareTo(max.getNama()) < 0) { // urutan alfabet lebih besar
                            max = copyDaftar[x]; // ketemu
                        }
                    }
                }
            }

            for (int z=0; z<totalElemenFasilkom; z++) { // hapus
                if (copyDaftar[z] == max) {
                    copyDaftar[z] = null;
                }
            }

            daftarUrutan[y] = max; // dimasukkan ke urutan
            max = new Dosen("Z"); // reset
        }

        // print urutan objek
        int nomor = 1;
        for (int x=0; x<totalElemenFasilkom; x++) {
            System.out.println(nomor + ". " + daftarUrutan[x].getNama() + "(" + daftarUrutan[x].friendship + ")");
            nomor++;
        }
    }

    static void programEnd() {
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada fasilkom");

        friendshipRanking();

    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}