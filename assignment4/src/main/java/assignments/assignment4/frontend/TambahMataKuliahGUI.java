package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{
    private JLabel labelJudul = new JLabel("Tambah Mata Kuliah");
    private JLabel labelKode = new JLabel("Kode Mata Kuliah:");
    private JLabel labelNama = new JLabel("Nama Mata Kuliah:");
    private JLabel labelSKS = new JLabel("SKS:");
    private JLabel labelKapasitas = new JLabel("Kapasitas:");
    private JTextField jftKode = new JTextField(20);
    private JTextField jftNama = new JTextField(20);
    private JTextField jftSKS = new JTextField(20);
    private JTextField jftKapasitas = new JTextField(20);
    private JButton jbtTambahkan = new JButton("Tambahkan");
    private JButton jbtKembali = new JButton("Kembali");

    JPanel panel1 = new JPanel();
    JPanel panel2 = new JPanel();
    JPanel panel3 = new JPanel();
    JPanel panel4 = new JPanel();
    JPanel panel5 = new JPanel();
    JPanel panel6 = new JPanel();

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // TODO: Implementasikan Tambah Mata Kuliah
        setPanel(frame);
        setKomponen();
        setListener(frame, daftarMahasiswa, daftarMataKuliah);
    }

    private void setPanel(JFrame frame) {
        JPanel frameTengah = new JPanel();
        frameTengah.setLayout(new GridLayout(11, 1, 0, 0)); // 11 rows and 1 column
        frame.setLayout(new BorderLayout(0, 30)); //vgap = 30

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        panel1.add(jftKode);
        panel2.add(jftNama);
        panel3.add(jftSKS);
        panel4.add(jftKapasitas);
        panel5.add(jbtTambahkan);
        panel6.add(jbtKembali);

        frameTengah.add(labelJudul);
        frameTengah.add(labelKode);
        frameTengah.add(panel1);
        frameTengah.add(labelNama);
        frameTengah.add(panel2);
        frameTengah.add(labelSKS);
        frameTengah.add(panel3);
        frameTengah.add(labelKapasitas);
        frameTengah.add(panel4);
        frameTengah.add(panel5);
        frameTengah.add(panel6);
    }

    private void setKomponen() {
        labelJudul.setFont(SistemAkademikGUI.fontTitle);
        labelKode.setFont(SistemAkademikGUI.fontGeneral);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        labelSKS.setFont(SistemAkademikGUI.fontGeneral);
        labelKapasitas.setFont(SistemAkademikGUI.fontGeneral);
        jbtTambahkan.setFont(SistemAkademikGUI.fontGeneral);
        jbtKembali.setFont(SistemAkademikGUI.fontGeneral);

        labelJudul.setHorizontalAlignment(JLabel.CENTER);
        labelKode.setHorizontalAlignment(JLabel.CENTER);
        labelNama.setHorizontalAlignment(JLabel.CENTER);
        labelSKS.setHorizontalAlignment(JLabel.CENTER);
        labelKapasitas.setHorizontalAlignment(JLabel.CENTER);

        jbtTambahkan.setBackground(SistemAkademikGUI.bg1);
        jbtKembali.setBackground(SistemAkademikGUI.bg2);

        jbtTambahkan.setForeground(SistemAkademikGUI.fg);
        jbtKembali.setForeground(SistemAkademikGUI.fg);

    }
    
    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        jbtTambahkan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String kode = jftKode.getText();
                String nama = jftNama.getText();
                String strSKS = jftSKS.getText();
                String strKapasitas = jftKapasitas.getText();
                int sks = (strSKS.trim().isEmpty()) ? 0 : Integer.parseInt(strSKS); // sks = 0 kalo kosong
                int kapasitas = (strKapasitas.trim().isEmpty()) ? 0 : Integer.parseInt(strKapasitas); // kapasitas = 0 kalo kosong
                String message;

                for (MataKuliah mataKuliah : daftarMataKuliah) {
                    if (mataKuliah.getNama().equals(nama)){
                        message = String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama);
                        JOptionPane.showMessageDialog(frame, message);
                        return;
                    }
                }
                
                if ((nama.trim().isEmpty()) || (kode.trim().isEmpty())
                    || (kapasitas == 0) || (sks == 0)) {
                    message = "Mohon isi seluruh Field";
                    JOptionPane.showMessageDialog(frame, message);
                    return;
                }

                daftarMataKuliah.add(new MataKuliah(kode, nama, sks, kapasitas));
                message = String.format("Mata Kuliah %s berhasil ditambahkan", nama);
                JOptionPane.showMessageDialog(frame, message);
            }
        });
    }
}
