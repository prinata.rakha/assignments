package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
        long a = npm / 1000000000000L; // ngambil digit pertama dan kedua
        long b = npm / 10000000000L % 100; // digit ke 1 sampe ke 4, lalu ambil dua digit terakhir
        long c = npm / 100 % 100000000; // digit ke 1 sampe ke 12, lalu ambil 8 digit terakhir

        // A
        long tahunMasuk = (long) 2000 + a;
        if (a < 10L) { // tahun masuk mahasiswa dimulai dr 2010
            return false;
        }

        // B
        // kode jurusan
        if (!((b == 01L) || (b == 02L) || (b == 3L) || (b == 11L) || (b == 12L))) {
            return false;
        }

        // C
        long dd = c / 1000000;
        long mm = c / 10000 % 100;
        long yyyy = c % 10000;

        //cek tanggal dan bulan bener atau ga
        //cek umur mahasiswa harus lebih atau sama dengan 15
        if (!(((dd > 0)&&(dd <= 31)) && ((mm > 0)&&(mm <= 12)) && ((tahunMasuk - yyyy >= 15)))) {
            return false;
        }

        // E
        long e = npm / 10;
        long digitTerakhir = npm % 10;

        long hasil = 0;
        hasil += ((e/1000000000000L) * (e%10)); // digit 1 dikali digit 13
        hasil += ((e/100000000000L%10) * (e%100/10)); // digit 2 * digit 12
        hasil += ((e/10000000000L%10) * (e%1000/100)); // digit 3 * digit 11
        hasil += ((e/1000000000%10) * (e%10000/1000)); // digit 4 * digit 10
        hasil += ((e/100000000%10) * (e%100000/10000)); // digit 5 * diigit 9
        hasil += ((e/10000000%10) * (e%1000000/100000)); // digit 6 * digit 8

        long kode = satuDigit(hasil); //hasilnya harus 1 digit, kalo lebih (10, 11, ...) digitnya harus ditambahin

        if (kode != digitTerakhir) {
            return false;
        }

        // valid atau ga?
        return true;
    } 
    public static long satuDigit(long x) {
        long hasil = x;
        if (x>=10) {
            hasil = (x/10) + (x%10); //kedua digit ditambahin
        }

        if (hasil >= 10) { //rekursif
            return satuDigit(hasil);
        } else { //basecase
            return hasil;
        }
    }
    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format
        // sama kyk di method validate
        long a = npm / 1000000000000L;
        long b = npm / 10000000000L % 100;
        long c = npm / 100 % 100000000;
        String jurusan;

        long tahunMasuk = (long) 2000 + a;

        // kode jurusan
        if (b == 01) {
            jurusan = "Ilmu Komputer";
        } else if (b == 02) {
            jurusan = "Sistem Informasi";
        } else if (b == 03) {
            jurusan = "Teknologi Informasi";
        } else if (b == 11) {
            jurusan = "Teknik Telekomunikasi";
        } else { // b == 12
            jurusan = "Teknik Elektro";
        }

        long dd = c / 1000000;
        long mm = c / 10000 % 100;
        long yyyy = c % 10000;

        String hasil = "Tahun masuk: " + tahunMasuk + "\nJurusan: " + jurusan + "\nTanggal Lahir: ";

        // ex: 07, 08, 31, dll
        if (dd < 10) {
            hasil = hasil + "0" + dd; //0X
        } else {
            hasil = hasil + dd; //XX
        }

        // ex: 01, 05, 12, dll
        if (mm < 10) {
            hasil = hasil + "-0" + mm; 
        } else {
            hasil = hasil + "-" + mm; 
        }

        hasil = hasil + "-" + yyyy;

        return hasil;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // TODO: Check validate and extract NPM
            if (validate(npm)) {
                System.out.println(extract(npm));
            } else {
                System.out.println("NPM tidak valid!");
            }

            System.out.println();
            
        }
        input.close();
    }
}