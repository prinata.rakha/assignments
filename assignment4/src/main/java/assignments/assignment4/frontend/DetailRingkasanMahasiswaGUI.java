package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    private JLabel labelJudul = new JLabel("Detail Ringkasan Mahasiswa");
    private JLabel labelNama = new JLabel();
    private JLabel labelNPM = new JLabel();
    private JLabel labelJurusan = new JLabel();
    private JLabel labelDaftarMatkul = new JLabel("Daftar Mata Kuliah:");
    private JLabel labelMatkulGaAda = new JLabel("Belum ada mata kuliah yang diambil");
    private JLabel labelSKS = new JLabel();
    private JLabel labelHasilIRS = new JLabel("Hasil Pengecekan IRS:");
    private JLabel labelIRSAman = new JLabel("IRS tidak bermasalah");
    private JButton jbtSelesai = new JButton("Selesai");

    private JPanel panelMatkul = new JPanel();
    private JPanel panelIRS = new JPanel();
    private JPanel panelSelesai = new JPanel();

    private Font fontUrutan = new Font("Times New Roman", Font.BOLD, 11);



    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, 
            ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mahasiswa
        setPanel(frame, mahasiswa);
        setKomponen(mahasiswa, daftarMahasiswa, daftarMataKuliah);
        setListener(frame, daftarMahasiswa, daftarMataKuliah);

    }

    private void setPanel(JFrame frame, Mahasiswa mahasiswa) {
        JPanel frameTengah = new JPanel();
        frameTengah.setLayout(new GridLayout(10, 1, 0, 0)); // 10 rows and 1 column
        frame.setLayout(new BorderLayout(0, 30)); //vgap = 30

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        frameTengah.add(labelJudul);
        frameTengah.add(labelNama);
        frameTengah.add(labelNPM);
        frameTengah.add(labelJurusan);
        frameTengah.add(labelDaftarMatkul);
        frameTengah.add(panelMatkul);
        frameTengah.add(labelSKS);
        frameTengah.add(labelHasilIRS);
        frameTengah.add(panelIRS);
        frameTengah.add(panelSelesai);

        setUrutanMatkuldanIRS(mahasiswa);
        
        panelSelesai.add(jbtSelesai);
    }

    private void setUrutanMatkuldanIRS(Mahasiswa mahasiswa) {
        if (mahasiswa.getBanyakMatkul() != 0) {
            int row = mahasiswa.getBanyakMatkul();
            panelMatkul.setLayout(new GridLayout(row, 1, 0, 5));

            String[] urutanMatkul = getUrutanMatkul(mahasiswa);
            int nomor = 1;
            String namaMatkul; // 1. DDP --Next loop--> 2.Matdis --> ...
            for (int x=0; x<urutanMatkul.length; x++) {
                namaMatkul = (nomor++) + ". " + urutanMatkul[x];
                JLabel text = new JLabel(namaMatkul);
                text.setHorizontalAlignment(JLabel.CENTER);
                text.setFont(fontUrutan);
                panelMatkul.add(text);
            }
        } else {
            panelMatkul.add(labelMatkulGaAda);
            labelMatkulGaAda.setFont(fontUrutan);
        }



        mahasiswa.cekIRS();
        if (mahasiswa.getBanyakMasalahIRS() != 0) {
            int row = mahasiswa.getBanyakMasalahIRS();
            panelIRS.setLayout(new GridLayout(row, 1, 0, 5));

            String [] masalahIRS = mahasiswa.getMasalahIRS();
            int nomor = 1;
            String irs;
            for (int x=0; x<mahasiswa.getBanyakMasalahIRS(); x++) {
                irs = (nomor++) + ". " + masalahIRS[x];
                JLabel text = new JLabel(irs);
                text.setHorizontalAlignment(JLabel.CENTER);
                text.setFont(fontUrutan);
                panelIRS.add(text);
            }    
        } else {
            panelIRS.add(labelIRSAman);
            labelIRSAman.setFont(fontUrutan);
        }
    }

    private void setKomponen(Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        String nama = "Nama: " + mahasiswa.getNama();
        String npm = "NPM: " + mahasiswa.getNpm();
        String jurusan = "Jurusan: " + mahasiswa.getJurusan();
        String sks = "Total SKS: " + mahasiswa.getTotalSKS();

        labelNama.setText(nama);
        labelNPM.setText(npm);
        labelJurusan.setText(jurusan);
        labelSKS.setText(sks);
        
        labelJudul.setFont(SistemAkademikGUI.fontTitle);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        labelJurusan.setFont(SistemAkademikGUI.fontGeneral);
        labelDaftarMatkul.setFont(SistemAkademikGUI.fontGeneral);
        labelSKS.setFont(SistemAkademikGUI.fontGeneral);
        labelHasilIRS.setFont(SistemAkademikGUI.fontGeneral);
        jbtSelesai.setFont(SistemAkademikGUI.fontGeneral);

        labelJudul.setHorizontalAlignment(JLabel.CENTER);
        labelNama.setHorizontalAlignment(JLabel.CENTER);
        labelNPM.setHorizontalAlignment(JLabel.CENTER);
        labelJurusan.setHorizontalAlignment(JLabel.CENTER);
        labelDaftarMatkul.setHorizontalAlignment(JLabel.CENTER);
        labelSKS.setHorizontalAlignment(JLabel.CENTER);
        labelHasilIRS.setHorizontalAlignment(JLabel.CENTER);

        jbtSelesai.setBackground(SistemAkademikGUI.bg1);
        jbtSelesai.setForeground(SistemAkademikGUI.fg);
    }

    private String[] getUrutanMatkul(Mahasiswa mahasiswa) {
        MataKuliah[] daftarMatkul = mahasiswa.getMataKuliah();
        String[] daftarNamaMatkul = new String[mahasiswa.getBanyakMatkul()]; // daftarMatkul versi nama matkulnya doang
        String[] temp = new String[mahasiswa.getBanyakMatkul()]; // deep copy daftarNamaMatkul
        for (int x=0; x<mahasiswa.getBanyakMatkul(); x++) {
            daftarNamaMatkul[x] = daftarMatkul[x].getNama();
            temp[x] = daftarMatkul[x].getNama();
        }

        String[] urutanNamaMatkul = new String[mahasiswa.getBanyakMatkul()]; // daftarNamaMatkul versi terurut secara alfabet

        String min = temp[0];
        int indexMinSementara = 0;
        for (int y=0; y<urutanNamaMatkul.length; y++) { // ngurutin dr alfabet terkecil ke besar. ex: a, b, c, ...
            for (int x=0; x<temp.length; x++) { // mencari nama terkecil dalam array temp di setiap loop
                if ((temp[x] != null) && (temp[x].compareToIgnoreCase(min) < 0)) {
                    min = temp[x];
                    indexMinSementara = x;
                }
            }
            temp[indexMinSementara] = null; // apus
            urutanNamaMatkul[y] = min;

            int index = 0;
            min = temp[index];
            while ((index<temp.length) && (min == null)) { // set min ke elemen yg belom di apus (dijadiin null)
                min = temp[index++];
            }
        }

        return urutanNamaMatkul;

    }

    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtSelesai.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }

}
