package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {
    private JLabel labelJudul = new JLabel("Ringkasan Mahasiswa");
    private JLabel labelNPM = new JLabel("Pilih NPM");
    private JComboBox jcbNPM = new JComboBox();
    private JButton jbtLihat = new JButton("Lihat");
    private JButton jbtKembali = new JButton("Kembali");

    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mahasiswa
        setPanel(frame);
        setKomponen(daftarMahasiswa, daftarMataKuliah);
        setListener(frame, daftarMahasiswa, daftarMataKuliah);

    }

    private void setPanel(JFrame frame) {
        JPanel frameTengah = new JPanel();
        frameTengah.setLayout(new GridLayout(5, 1, 0, 0)); // 5 rows and 1 column
        frame.setLayout(new BorderLayout(0, 100)); //vgap = 100

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        frameTengah.add(labelJudul);
        frameTengah.add(labelNPM);
        frameTengah.add(panel1);
        frameTengah.add(panel2);
        frameTengah.add(panel3);

        panel1.add(jcbNPM);
        panel2.add(jbtLihat);
        panel3.add(jbtKembali);

    }

    private void setKomponen(ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        setComboBoxNPM(daftarMahasiswa);

        labelJudul.setFont(SistemAkademikGUI.fontTitle);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        jcbNPM.setFont(SistemAkademikGUI.fontGeneral);
        jbtLihat.setFont(SistemAkademikGUI.fontGeneral);
        jbtKembali.setFont(SistemAkademikGUI.fontGeneral);

        labelJudul.setHorizontalAlignment(JLabel.CENTER);
        labelNPM.setHorizontalAlignment(JLabel.CENTER);

        jbtLihat.setBackground(SistemAkademikGUI.bg1);
        jbtKembali.setBackground(SistemAkademikGUI.bg2);

        jbtLihat.setForeground(SistemAkademikGUI.fg);
        jbtKembali.setForeground(SistemAkademikGUI.fg);
 
    }

    private void setComboBoxNPM(ArrayList<Mahasiswa> daftarMahasiswa) {
        long[] urutanNPM = new long[daftarMahasiswa.size()];
        long[] temp = new long[daftarMahasiswa.size()]; 
        int index = 0;
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            temp[index++] = mahasiswa.getNpm();
        }

        long max = 0L;
        int indexMaxSementara = 0;
        for (int y=0; y<urutanNPM.length; y++) { // ngurutin dr terbesar ke terkecil. ex: 9, 8, 7, ...
            for (int x=0; x<temp.length; x++) {
                if (temp[x] > max) {
                    max = temp[x];
                    indexMaxSementara = x;
                }
            }
            temp[indexMaxSementara] = 0L; // apus
            urutanNPM[y] = max;
            max = 0L; // reset nilai max jd terkecil
        }

        for (int z=(urutanNPM.length - 1); z>=0; z--) {
            jcbNPM.addItem(urutanNPM[z]);
        }
    }

    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        jbtLihat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jcbNPM.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }

                long npm = ((Long)jcbNPM.getSelectedItem()).longValue();
                Mahasiswa mahasiswa = getMahasiswa(npm, daftarMahasiswa);

                frame.getContentPane().removeAll();
                frame.repaint();
                new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }

    // Uncomment method di bawah jika diperlukan
    
    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
}
