package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    MataKuliah mataKuliah;

    Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("Dosen", nama);
    }

    MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */    
        if (this.mataKuliah != null) { //udh ngajar
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s%n", getNama(), this.mataKuliah.getNama());
        } else if (mataKuliah.getDosen() != null) { // udh ada pengajar
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar%n", mataKuliah.getNama());
        } else { // mengajar matkul
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.println(getNama() + " mengajar mata kuliah " + mataKuliah.getNama());
        }
    }

    void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        if (mataKuliah == null) {
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun%n", getNama());
        } else {
            System.out.printf("%s berhenti mengajar %s%n", getNama(), mataKuliah);
            mataKuliah.dropDosen();
            mataKuliah = null;
        }
    }
}