package assignments.assignment3;

abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    String tipe;
    
    String nama;

    int friendship;

    ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    int jumlahDisapa = 0;

    //constructor
    ElemenFasilkom(String tipe, String nama) {
        this.tipe = tipe;
        this.nama = nama;
    }

    public String getNama() {
        return this.nama;
    }
    public String getTipe() {
        return this.tipe;
    }
    public int getJumlahDisapa() {
        return this.jumlahDisapa;
    }
    void getOrangygDisapa() { // buat debugging doang, sebenrnya gakepake di soal
        for (int x=0; x<jumlahDisapa; x++) {
            System.out.print(telahMenyapa[x].getNama() + ", ");
        }
        System.out.println();
    }

    void addTelahMenyapa(ElemenFasilkom elemenFasilkom) {
        int index = 0;
        for (int x=0; x<telahMenyapa.length; x++) { // mencari index untuk di isi nilainya dengan objek baru
            if (telahMenyapa[x] == null) {
                index = x;
                break;
            }
        }

        telahMenyapa[index] = elemenFasilkom;
        jumlahDisapa++;
    }

    void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        for (int x=0; x<jumlahDisapa; x++) {
            if (elemenFasilkom.equals(telahMenyapa[x])) { // udh pernah disapa
                System.out.printf("[DITOLAK] %s telah menyapa %s hari ini%n", this.nama, elemenFasilkom.getNama());
                return;
            }
        }

        // blom pernah disapa

        boolean hubunganMahasiswaDosen = false;

        if ((this.tipe.equals("Mahasiswa")) && (elemenFasilkom.getTipe().equals("Dosen"))) { // mahasiswa nyapa dosen

            for (int x=0; x< ((Mahasiswa)this).getJumlahMatkul() ; x++ ) {
                if (((Dosen)elemenFasilkom).getMataKuliah() == null) { // dosen ga ngajar
                    break;
                }
                else if (((Mahasiswa)this).getMataKuliah()[x].equals(((Dosen)elemenFasilkom).getMataKuliah())) { // satu matkul
                    hubunganMahasiswaDosen = true;
                    break;
                }
            }
        } else if ((this.tipe.equals("Dosen")) && (elemenFasilkom.getTipe().equals("Mahasiswa"))) { // dosen nyapa mahasiswa

            for (int x=0; x< ((Mahasiswa)elemenFasilkom).getJumlahMatkul() ; x++ ) {
                if (((Dosen)this).getMataKuliah() == null) { // dosen ga ngajar
                    break;
                }
                else if (((Mahasiswa)elemenFasilkom).getMataKuliah()[x].equals(((Dosen)this).getMataKuliah())) { // satu matkul
                    hubunganMahasiswaDosen = true;
                    break;
                }
            }
        }

        if (hubunganMahasiswaDosen) { // ada hubungan mahasiswa dengan dosen ataupun sebaliknya
            this.friendship += 2;
            elemenFasilkom.friendship += 2;
        }

        elemenFasilkom.addTelahMenyapa(this); // yg disapa juga menyapa balik
        addTelahMenyapa(elemenFasilkom);
        System.out.printf("%s menyapa dengan %s%n", this.nama, elemenFasilkom.getNama());
    }

    void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom[] reset = new ElemenFasilkom[100];
        this.telahMenyapa = reset;
        this.jumlahDisapa = 0;
    }

    void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        if (penjual.getTipe().equals("ElemenKantin")) {
            boolean ada = false;
            ElemenKantin penjualKantin = (ElemenKantin)penjual; // casting
            Makanan makanan = null;
            for (int x=0; x<penjualKantin.getJumlahMakanan(); x++) {
                if (penjualKantin.getDaftarMakanan()[x].getNama().equals(namaMakanan)) { // dapet makanannya
                    ada = true;
                    makanan = penjualKantin.getDaftarMakanan()[x]; 
                } else ;
            }

            if (ada) { // makannannya ada
                System.out.printf("%s berhasil membeli %s seharga %s%n", pembeli.getNama(), makanan.getNama(), String.valueOf(makanan.getHarga()));
                pembeli.friendship++;
                penjual.friendship++;
            } else { // gada makanan
                System.out.printf("[DITOLAK] %s tidak menjual %s%n", penjual.getNama(), namaMakanan);
            }

        } else { // penjual bukan elemen kantin
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin%n", penjual.getNama());
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
    
    public boolean equals(ElemenFasilkom other) {
        if (other == null) {return false;}
        // membandingkan nama dan tipenya
        return (this.getNama().equals(other.getNama()) && this.getTipe().equals(other.getTipe()));
    }
}