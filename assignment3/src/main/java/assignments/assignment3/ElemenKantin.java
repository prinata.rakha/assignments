package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    Makanan[] daftarMakanan = new Makanan[10];

    int jumlahMakanan = 0;

    Makanan[] getDaftarMakanan() {
        return this.daftarMakanan;
    }
    int getJumlahMakanan() {
        return this.jumlahMakanan;
    }

    ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("ElemenKantin", nama);
    }

    void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        int index = 0;
        Makanan makanan = new Makanan(nama, harga);
        for (int x=0; x<daftarMakanan.length; x++) { // cari index yg kosong (null)
            if (daftarMakanan[x] == null) {
                index = x; // ketemu
                jumlahMakanan++;
                break;
            } else if (daftarMakanan[x].equals(makanan)) {
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar%n", nama);
                return;
            } else ;
        }

        daftarMakanan[index] = makanan;
        System.out.println(getNama() + " telah mendaftarkan makanan " + nama + " dengan harga " + harga);
    }
}