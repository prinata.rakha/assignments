package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    MataKuliah[] daftarMataKuliah = new MataKuliah[10];

    int jumlahMatkul = 0;
    
    long npm;

    String tanggalLahir;
    
    String jurusan;

    Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        super("Mahasiswa", nama);
        this.npm = npm;
        this.jurusan = extractJurusan(npm);
        this.tanggalLahir = extractTanggalLahir(npm);
    }

    public long getNpm() {
        return this.npm;
    }
    public MataKuliah[] getMataKuliah() {
        return this.daftarMataKuliah;
    }
    public int getJumlahMatkul() {
        return this.jumlahMatkul;
    }

    void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        int index = 0;

        for (int x=0; x<daftarMataKuliah.length; x++) { // cari index yg isinya masih kosong (null)
            if ((daftarMataKuliah[x] != null) && (daftarMataKuliah[x].equals(mataKuliah))) { // udh pernah diambil
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya%n", mataKuliah.getNama());
                return;
            } else if (daftarMataKuliah[x] == null) {
                index = x; // ketemu
                break;
            } else ;
        }

        if (mataKuliah.getKapasitas() == mataKuliah.getJumlahMahasiswa()) { // kapasitas penuh
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.%n", mataKuliah.getNama());
        } else { // berhasil nambahin
            daftarMataKuliah[index] = mataKuliah;
            mataKuliah.addMahasiswa(this);
            System.out.printf("%s berhasil menambahkan mata kuliah %s%n", getNama(), mataKuliah.getNama());
            this.jumlahMatkul++;
        }
    }

    void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        for (int x=0; x<daftarMataKuliah.length; x++) { // cari objek matkul
            if ((daftarMataKuliah[x] != null) && (daftarMataKuliah[x].equals(mataKuliah))) { // ketemu objeknya
                daftarMataKuliah[x] = null; // hapus objek
                mataKuliah.dropMahasiswa(this); // ngapus objek mahasiswa di matkulnya
                System.out.printf("%s berhasil drop mata kuliah %s%n", getNama(), mataKuliah.getNama());
                return;
            }
        }

        System.out.printf("[DITOLAK] %s belum pernah diambil%n", mataKuliah.getNama());
    }

    String extractTanggalLahir(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        long c = npm / 100 % 100000000; // digit ke 1 sampe ke 12, lalu ambil 8 digit terakhir
        long dd = c / 1000000; // 2 digit pertama dr c
        long mm = c / 10000 % 100; // 4 digit pertama dr c, lalu ambil dua digit terakhirnya
        long yyyy = c % 10000; // 4 digit terakhir dari c

        String tanggalLahir = dd + "-" + mm + "-" + yyyy;

        return tanggalLahir;
    }

    String extractJurusan(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        long b = npm / 10000000000L % 100; // digit ke 1 sampe ke 4, lalu ambil dua digit terakhir
        String jurusan = (b == 01) ? "Ilmu Komputer" : "Sistem Informasi";
        return jurusan;
    }

    public boolean equals(Mahasiswa other) {
        // bandingin nama, npm
        return (this.getNama().equals(other.getNama()) && (this.getNpm() == (other.getNpm())));
    }
}