package assignments.assignment2;

import java.util.*;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS = new String[19];
    private int totalSKS; //max 24
    private String nama;
    private String jurusan;
    private long npm;
    private int totalMataKuliah = 0;
    private int totalMasalahIRS = 0;

    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
        long kodeJurusan = npm / 10000000000L % 100L;
        this.jurusan = (kodeJurusan == 01) ? "Ilmu Komputer" : "Sistem Informasi";
    }

    // setter & getter
    public MataKuliah[] getMataKuliah() {
        return this.mataKuliah;
    }
    public void setMataKuliah(MataKuliah[] baru) {
        this.mataKuliah = baru;
    }
    public String[] getMasalahIRS() {
        return this.masalahIRS;
    }
    public int getTotalSKS() {
        return this.totalSKS;
    }
    public String getNama() {
        return this.nama;
    }
    public String getJurusan() {
        return this.jurusan;
    }
    public long getNPM() {
        return this.npm;
    }
    public int getTotalMataKuliah() {
        return this.totalMataKuliah;
    }
    public int getTotalMasalahIRS() {
        return this.totalMasalahIRS;
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        for (int x=0; x < 10; x++) {
            if (this.mataKuliah[x] == mataKuliah) { // ii.
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya.");
                return;
            } else if (mataKuliah.getTotalMahasiswa() == mataKuliah.getKapasitas() ) { // iii. kapasitas penuh
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya.");
                return;
            }
        }
        if (getTotalMataKuliah() == 10) { // i.
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
            return;
        }

        // lolos seleksi, baru di add
        this.mataKuliah[this.totalMataKuliah++] = mataKuliah;
        this.totalSKS += mataKuliah.getSKS();
        Mahasiswa mahasiswa = new Mahasiswa(this.nama, this.npm);
        mataKuliah.addMahasiswa(mahasiswa);
        cekIRS();
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        int index = -1;
        MataKuliah[] copyArray = new MataKuliah[10];

        for (int x=0; x < 10; x++) { // nyari index matkulnya
            if (getMataKuliah()[x] == mataKuliah) {
                index = x;
                break;
            }
        }

        if (index == -1) { // matkulnya blom pernah diambil
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " belum pernah diambil.");
            return;
        }

        this.totalSKS -= mataKuliah.getSKS();

        for (int x=0; x < 9; x++) { // buat array baru tanpa matkul yg diapus
            if (x < index) {
                copyArray[x] = getMataKuliah()[x];
            } else {
                copyArray[x] = getMataKuliah()[x+1];
            }
        }
        setMataKuliah(copyArray);
        this.totalMataKuliah--;
        Mahasiswa mahasiswa = new Mahasiswa(this.nama, this.npm);
        mataKuliah.dropMahasiswa(mahasiswa);
        cekIRS();
    }

    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        // max 24 sks  
        if (getTotalSKS() > 24) {
            addMasalahIRS("SKS yang Anda ambil lebih dari 24");
        }

        // IK cmn boleh diambil jurusan ilkom
        // SI cmn boleh diambil jurusan si
        for (int x=0; x < this.totalMataKuliah; x++) {
            if (this.mataKuliah[x].getKode().equals("IK")) {
                if (getJurusan().equals("Sistem Informasi")) {
                    addMasalahIRS("Mata Kuliah " + this.mataKuliah[x].getNama() + " tidak dapat diambil jurusan SI");
                }
            } else if (this.mataKuliah[x].getKode().equals("SI")) {
                if (getJurusan().equals("Ilmu Komputer")) {
                    addMasalahIRS("Mata Kuliah " + this.mataKuliah[x].getNama() + " tidak dapat diambil jurusan IK");
                }
            } 
        }
    }
    public void addMasalahIRS(String masalah) {
        for (int x=0; x<this.totalMasalahIRS; x++) {
            if (this.masalahIRS[x].equals(masalah)) { //udh di catet
                return; // gausa ditambahin lagi
            }
        }
        if (this.totalMasalahIRS >= this.masalahIRS.length) { // array full
            this.masalahIRS = Arrays.copyOf(this.masalahIRS, 4* this.masalahIRS.length);
        } 
        this.masalahIRS[this.totalMasalahIRS++] = masalah;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return getNama();
    }

}
