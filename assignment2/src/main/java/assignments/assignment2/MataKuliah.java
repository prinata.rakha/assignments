package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int totalMahasiswa = 0;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public String getKode() {
        return this.kode;
    }
    public String getNama() {
        return this.nama;
    }
    public int getSKS() {
        return this.sks;
    }
    public int getKapasitas() {
        return this.kapasitas;
    }
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }
    public int getTotalMahasiswa() {
        return this.totalMahasiswa;
    }
    public void setDaftarMahasiswa(Mahasiswa[] baru) {
        this.daftarMahasiswa = baru;
    }



    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa[this.totalMahasiswa++] = mahasiswa;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        int index = -1;
        for (int x=0; x < this.daftarMahasiswa.length; x++) {
            if (this.daftarMahasiswa[x] == mahasiswa) {
                index = x;
                break;
            }
        }

        Mahasiswa[] copy = new Mahasiswa[this.kapasitas];
        for (int x=0; x < this.daftarMahasiswa.length-1; x++) {
            if (x == index) {
                copy[x] = this.daftarMahasiswa[x+1];
            } else {
                copy[x] = this.daftarMahasiswa[x];
            }
        }

        this.totalMahasiswa--;
        setDaftarMahasiswa(copy);

    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
}
