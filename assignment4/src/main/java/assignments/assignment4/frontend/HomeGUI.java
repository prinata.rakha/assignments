package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    private JLabel titleLabel = new JLabel();

    private JButton jbt1 = new JButton("Tambah Mahasiswa");
    private JButton jbt2 = new JButton("Tambah Mata Kuliah");
    private JButton jbt3 = new JButton("Tambah IRS");
    private JButton jbt4 = new JButton("Hapus IRS");
    private JButton jbt5 = new JButton("Lihat Ringkasan Mahasiswa");
    private JButton jbt6 = new JButton("Lihat Ringkasan Mata Kuliah");

    private JPanel frameTengah = new JPanel(); 

    // panel untuk setiap button sehingga button dapat ditentukan ukurannya
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();
    private JPanel panel5 = new JPanel();
    private JPanel panel6 = new JPanel();
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        // TODO: Implementasikan Halaman Home
        setPanel(frame);
        setKomponen();
        setListener(frame, daftarMahasiswa, daftarMataKuliah);
    }

    private void setPanel(JFrame frame) {
        frame.setLayout(new BorderLayout(0, 50)); //vgap = 50

        // agar label dan button terlihat berada di tengah window
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        // buat 7 row label dan button
        frameTengah.setLayout(new GridLayout(7, 1, 0, 0)); // 7 rows, 1 column, hgap=vgap=5
        
        frameTengah.add(titleLabel);
        frameTengah.add(panel1);
        frameTengah.add(panel2);
        frameTengah.add(panel3);
        frameTengah.add(panel4);
        frameTengah.add(panel5);
        frameTengah.add(panel6);

        panel1.add(jbt1);
        panel2.add(jbt2);
        panel3.add(jbt3);
        panel4.add(jbt4);
        panel5.add(jbt5);
        panel6.add(jbt6);
    }

    private void setKomponen() {
        jbt1.setFont(SistemAkademikGUI.fontGeneral);
        jbt2.setFont(SistemAkademikGUI.fontGeneral);
        jbt3.setFont(SistemAkademikGUI.fontGeneral);
        jbt4.setFont(SistemAkademikGUI.fontGeneral);
        jbt5.setFont(SistemAkademikGUI.fontGeneral);
        jbt6.setFont(SistemAkademikGUI.fontGeneral);

        jbt1.setForeground(SistemAkademikGUI.fg);
        jbt2.setForeground(SistemAkademikGUI.fg);
        jbt3.setForeground(SistemAkademikGUI.fg);
        jbt4.setForeground(SistemAkademikGUI.fg);
        jbt5.setForeground(SistemAkademikGUI.fg);
        jbt6.setForeground(SistemAkademikGUI.fg);

        jbt1.setBackground(SistemAkademikGUI.bg1);
        jbt2.setBackground(SistemAkademikGUI.bg1);
        jbt3.setBackground(SistemAkademikGUI.bg1);
        jbt4.setBackground(SistemAkademikGUI.bg1);
        jbt5.setBackground(SistemAkademikGUI.bg1);
        jbt6.setBackground(SistemAkademikGUI.bg1);

        jbt1.setPreferredSize(new Dimension(180,35));
        jbt2.setPreferredSize(new Dimension(200,35));
        jbt3.setPreferredSize(new Dimension(120,35));
        jbt4.setPreferredSize(new Dimension(110,35));
        jbt5.setPreferredSize(new Dimension(240,35));
        jbt6.setPreferredSize(new Dimension(250,35));
    }

    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbt1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        jbt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        jbt3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                // frame.add(new JLabel("tes"));
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                
            }
        });
        jbt4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        jbt5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        jbt6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }
}
