package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {
    private JLabel labelJudul = new JLabel("Hapus IRS");
    private JLabel labelNPM = new JLabel("Pilih NPM");
    private JLabel labelMatkul = new JLabel("Pilih Nama Matkul");
    private JComboBox jcbNPM = new JComboBox();
    private JComboBox jcbMatkul = new JComboBox();
    private JButton jbtHapus = new JButton("Hapus");
    private JButton jbtKembali = new JButton("Kembali");

    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Hapus IRS
        setPanel(frame);
        setKomponen(daftarMahasiswa, daftarMataKuliah);
        setListener(frame, daftarMahasiswa, daftarMataKuliah);

    }

    private void setPanel(JFrame frame) {
        JPanel frameTengah = new JPanel();
        frameTengah.setLayout(new GridLayout(7, 1, 0, 0)); // 7 rows and 1 column
        frame.setLayout(new BorderLayout(0, 80)); //vgap = 80

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        frameTengah.add(labelJudul);
        frameTengah.add(labelNPM);
        frameTengah.add(panel1);
        frameTengah.add(labelMatkul);
        frameTengah.add(panel2);
        frameTengah.add(panel3);
        frameTengah.add(panel4);

        panel1.add(jcbNPM);
        panel2.add(jcbMatkul);
        panel3.add(jbtHapus);
        panel4.add(jbtKembali);

    }

    private void setKomponen(ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        setComboBoxNPM(daftarMahasiswa);
        setComboBoxMatkul(daftarMataKuliah);
        
        labelJudul.setFont(SistemAkademikGUI.fontTitle);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        labelMatkul.setFont(SistemAkademikGUI.fontGeneral);
        jcbNPM.setFont(SistemAkademikGUI.fontGeneral);
        jcbMatkul.setFont(SistemAkademikGUI.fontGeneral);
        jbtHapus.setFont(SistemAkademikGUI.fontGeneral);
        jbtKembali.setFont(SistemAkademikGUI.fontGeneral);

        labelJudul.setHorizontalAlignment(JLabel.CENTER);
        labelNPM.setHorizontalAlignment(JLabel.CENTER);
        labelMatkul.setHorizontalAlignment(JLabel.CENTER);

        jbtHapus.setBackground(SistemAkademikGUI.bg1);
        jbtKembali.setBackground(SistemAkademikGUI.bg2);

        jbtHapus.setForeground(SistemAkademikGUI.fg);
        jbtKembali.setForeground(SistemAkademikGUI.fg);

    }

    private void setComboBoxNPM(ArrayList<Mahasiswa> daftarMahasiswa) {
        long[] urutanNPM = new long[daftarMahasiswa.size()];
        long[] temp = new long[daftarMahasiswa.size()]; 
        int index = 0;
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            temp[index++] = mahasiswa.getNpm();
        }

        long max = 0L;
        int indexMaxSementara = 0;
        for (int y=0; y<urutanNPM.length; y++) { // ngurutin dr terbesar ke terkecil. ex: 9, 8, 7, ...
            for (int x=0; x<temp.length; x++) {
                if (temp[x] > max) {
                    max = temp[x];
                    indexMaxSementara = x;
                }
            }
            temp[indexMaxSementara] = 0L; // apus
            urutanNPM[y] = max;
            max = 0L; // reset nilai max jd terkecil
        }

        for (int z=(urutanNPM.length - 1); z>=0; z--) {
            jcbNPM.addItem(urutanNPM[z]);
        }
    }

    private void setComboBoxMatkul(ArrayList<MataKuliah> daftarMataKuliah) {
        if (daftarMataKuliah.isEmpty()) {return;}

        String[] urutanMatkul = new String[daftarMataKuliah.size()];
        String[] temp = new String[daftarMataKuliah.size()]; 
        int index = 0;
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            temp[index++] = mataKuliah.getNama();
        }

        String min = temp[0];
        int indexMinSementara = 0;
        for (int y=0; y<urutanMatkul.length; y++) { // ngurutin dr alfabet terkecil ke besar. ex: a, b, c, ...
            for (int x=0; x<temp.length; x++) {
                if ((temp[x] != null) && (temp[x].compareToIgnoreCase(min) < 0)) {
                    min = temp[x];
                    indexMinSementara = x;
                }
            }
            temp[indexMinSementara] = null; // apus
            urutanMatkul[y] = min;
            min = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"; // set string ke nilai terbesar
        }

        for (int z=0; z<urutanMatkul.length; z++) {
            jcbMatkul.addItem(urutanMatkul[z]);
        }
    }

    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        jbtHapus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((jcbNPM.getSelectedItem() == null) || (jcbMatkul.getSelectedItem() == null)) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }

                long npm = ((Long)jcbNPM.getSelectedItem()).longValue();
                Mahasiswa mahasiswa = getMahasiswa(npm, daftarMahasiswa);

                String nama = String.valueOf(jcbMatkul.getSelectedItem());
                MataKuliah mataKuliah = getMataKuliah(nama, daftarMataKuliah);

                String message = mahasiswa.dropMatkul(mataKuliah);
                JOptionPane.showMessageDialog(frame, message);

            }
        });
    }

    // Uncomment method di bawah jika diperlukan
    
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
}
