package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{
    private JLabel title = new JLabel("Tambah Mahasiswa");
    private JLabel labelNama = new JLabel("Nama:");
    private JLabel labelNPM = new JLabel("NPM:");
    private JTextField jftNama = new JTextField(20);
    private JTextField jftNPM = new JTextField(20);
    private JButton jbtTambahkan = new JButton("Tambahkan");
    private JButton jbtKembali = new JButton("Kembali");

    JPanel frameTengah = new JPanel();

    JPanel panel1 = new JPanel();
    JPanel panel2 = new JPanel();
    JPanel panel3 = new JPanel();
    JPanel panel4 = new JPanel();
    JPanel panel5 = new JPanel();
    JPanel panel6 = new JPanel();

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // TODO: Implementasikan Tambah Mahasiswa
        setPanel(frame);
        setKomponen();
        setListener(frame, daftarMahasiswa, daftarMataKuliah);
        
    }
    
    private void setPanel(JFrame frame) {
        frame.setLayout(new BorderLayout(0, 100)); //vgap = 100
        frameTengah.setLayout(new GridLayout(7, 1, 0, 0)); // 7 rows and 1 column

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        frameTengah.add(title);
        frameTengah.add(panel1);
        frameTengah.add(panel2);
        frameTengah.add(panel3);
        frameTengah.add(panel4);
        frameTengah.add(panel5);
        frameTengah.add(panel6);

        panel1.add(labelNama);
        panel2.add(jftNama);
        panel3.add(labelNPM);
        panel4.add(jftNPM);
        panel5.add(jbtTambahkan);
        panel6.add(jbtKembali);
    }

    private void setKomponen() {
        title.setFont(SistemAkademikGUI.fontTitle);
        labelNama.setFont(SistemAkademikGUI.fontGeneral);
        labelNPM.setFont(SistemAkademikGUI.fontGeneral);
        jftNama.setFont(SistemAkademikGUI.fontGeneral);
        jftNPM.setFont(SistemAkademikGUI.fontGeneral);
        jbtTambahkan.setFont(SistemAkademikGUI.fontGeneral);
        jbtKembali.setFont(SistemAkademikGUI.fontGeneral);

        title.setHorizontalAlignment(JLabel.CENTER);
        labelNama.setHorizontalAlignment(JLabel.CENTER);
        labelNPM.setHorizontalAlignment(JLabel.CENTER);
        
        jbtTambahkan.setBackground(SistemAkademikGUI.bg1);
        jbtKembali.setBackground(SistemAkademikGUI.bg2);

        jbtTambahkan.setForeground(SistemAkademikGUI.fg);
        jbtKembali.setForeground(SistemAkademikGUI.fg);

    }

    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        jbtTambahkan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nama = jftNama.getText();
                String strNPM = jftNPM.getText();
                long npm = (strNPM.trim().isEmpty()) ? 0 : Long.parseLong(strNPM); // npm = 0 kalo kosong
                String message;

                for (Mahasiswa mahasiswa : daftarMahasiswa) {
                    if (mahasiswa.getNpm() == npm){
                        message = String.format("NPM %s sudah pernah ditambahkan sebelumnya", String.valueOf(npm));
                        JOptionPane.showMessageDialog(frame, message);
                        return;
                    }
                }
                
                if ((nama.trim().isEmpty()) || (npm == 0)) {
                    message = "Mohon isi seluruh Field";
                    JOptionPane.showMessageDialog(frame, message);
                    return;
                }

                daftarMahasiswa.add(new Mahasiswa(nama, npm));
                message = String.format("Mahasiswa %s-%s berhasil ditambahkan", String.valueOf(npm), nama);
                JOptionPane.showMessageDialog(frame, message);
            }
        });
    }

}
