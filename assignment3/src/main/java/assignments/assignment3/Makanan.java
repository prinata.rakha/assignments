package assignments.assignment3;

class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    String nama;

    long harga;

    public String getNama() {
        return this.nama;
    }

    public long getHarga() {
        return this.harga;
    }

    Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public boolean equals(Makanan other) {
        // bandingin nama dan harga
        return (this.nama.equals(other.getNama()) && (this.harga == other.getHarga()));
    }
}