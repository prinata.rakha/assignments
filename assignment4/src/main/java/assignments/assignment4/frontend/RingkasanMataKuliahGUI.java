package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {
    private JLabel labelJudul = new JLabel("Ringkasan Mata Kuliah");
    private JLabel labelMatkul = new JLabel("Pilih Nama Matkul");
    private JComboBox jcbMatkul = new JComboBox();
    private JButton jbtLihat = new JButton("Lihat");
    private JButton jbtKembali = new JButton("Kembali");

    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mata Kuliah
        setPanel(frame);
        setKomponen(daftarMahasiswa, daftarMataKuliah);
        setListener(frame, daftarMahasiswa, daftarMataKuliah);

    }

    private void setPanel(JFrame frame) {
        JPanel frameTengah = new JPanel();
        frameTengah.setLayout(new GridLayout(5, 1, 0, 0)); // 5 rows and 1 column
        frame.setLayout(new BorderLayout(0, 100)); //vgap = 100

        // agar komponen terlihat berada di tengah window 
        frame.add(frameTengah, BorderLayout.CENTER);
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.SOUTH);

        frameTengah.add(labelJudul);
        frameTengah.add(labelMatkul);
        frameTengah.add(panel1);
        frameTengah.add(panel2);
        frameTengah.add(panel3);

        panel1.add(jcbMatkul);
        panel2.add(jbtLihat);
        panel3.add(jbtKembali);

    }

    private void setKomponen(ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        setComboBoxMatkul(daftarMataKuliah);

        labelJudul.setFont(SistemAkademikGUI.fontTitle);
        labelMatkul.setFont(SistemAkademikGUI.fontGeneral);
        jcbMatkul.setFont(SistemAkademikGUI.fontGeneral);
        jbtLihat.setFont(SistemAkademikGUI.fontGeneral);
        jbtKembali.setFont(SistemAkademikGUI.fontGeneral);

        labelJudul.setHorizontalAlignment(JLabel.CENTER);
        labelMatkul.setHorizontalAlignment(JLabel.CENTER);

        jbtLihat.setBackground(SistemAkademikGUI.bg1);
        jbtKembali.setBackground(SistemAkademikGUI.bg2);

        jbtLihat.setForeground(SistemAkademikGUI.fg);
        jbtKembali.setForeground(SistemAkademikGUI.fg);

    }

    private void setComboBoxMatkul(ArrayList<MataKuliah> daftarMataKuliah) {
        if (daftarMataKuliah.isEmpty()) {return;}

        String[] urutanMatkul = new String[daftarMataKuliah.size()];
        String[] temp = new String[daftarMataKuliah.size()]; 
        int index = 0;
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            temp[index++] = mataKuliah.getNama();
        }

        String min = temp[0];
        int indexMinSementara = 0;
        for (int y=0; y<urutanMatkul.length; y++) { // ngurutin dr alfabet terkecil ke besar. ex: a, b, c, ...
            for (int x=0; x<temp.length; x++) {
                if ((temp[x] != null) && (temp[x].compareToIgnoreCase(min) < 0)) {
                    min = temp[x];
                    indexMinSementara = x;
                }
            }
            temp[indexMinSementara] = null; // apus
            urutanMatkul[y] = min;
            min = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"; // set string ke nilai terbesar
        }

        for (int z=0; z<urutanMatkul.length; z++) {
            jcbMatkul.addItem(urutanMatkul[z]);
        }
    }

    private void setListener(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        jbtKembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed (ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        jbtLihat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jcbMatkul.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }

                String nama = String.valueOf(jcbMatkul.getSelectedItem());
                MataKuliah mataKuliah = getMataKuliah(nama, daftarMataKuliah);

                frame.getContentPane().removeAll();
                frame.repaint();
                new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }


    // Uncomment method di bawah jika diperlukan
    
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
}
